var Bicicleta = function (id, color, modelo, ubicacion) {
	this.id = id;
	this.color = color;
	this.modelo = modelo;
	this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function() {
	return 'id ' + this.id + "| color:" + this.color;
};

Bicicleta.allBicis = [];

Bicicleta.add = function(aBici){
	Bicicleta.allBicis.push(aBici);
}

Bicicleta.findById = function(aBiciId){
	//console.log(aBiciId);
	//console.log(Bicicleta.allBicis);
	//console.log(Bicicleta.allBicis.find(x => x.if == aBiciId));

	var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
	if (aBici)
		return aBici;
	else
		throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}

Bicicleta.removeById = function (aBiciId) {
	Bicicleta.findById(aBiciId);
	for (var i = 0; i < Bicicleta.allBicis.length; i++) {
		if (Bicicleta.allBicis[i].id == aBiciId){
			Bicicleta.allBicis.splice(i,1);
			break;
		}
	}
}

var a = new Bicicleta (1, 'rojo', 'urbana', [-33.44204, -70.63483]);
var b = new Bicicleta (2, 'blanca', 'urbana', [-33.44150, -70.63216]);
var c = new Bicicleta (3, 'naranja', 'urbana', [-33.44552, -70.63305]);

Bicicleta.add(a);
Bicicleta.add(b);
Bicicleta.add(c);

module.exports = Bicicleta;
